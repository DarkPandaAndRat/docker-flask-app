FROM centos
#RUN yum update -y && 

COPY requirements.txt .
COPY flask_app.py /opt/source_code/

RUN yum install python3 -y
RUN python3 -m pip install -r requirements.txt

#RUN pip3 install -r requirements.txt

ENTRYPOINT FLASK_APP=/opt/source_code/flask_app.py flask run --host=0.0.0.0
